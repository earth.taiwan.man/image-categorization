import re
from os import listdir
from shutil import copy
import pytesseract

pytesseract.pytesseract.tesseract_cmd = r'C:/Users/Your_Name/AppData/Local/Programs/Tesseract-OCR/tesseract.exe'

filenames = listdir('images')
origin = 'images/'
destination = 'categorized_result/'

# 在此僅提供一部分的程式碼。
for filename in filenames:
 ⋮
  single_output = re.sub(r'[\s\x21-\x2f\x3a-\x40\x5b-\x60\x7b-\x7e]', '', single_output)

  if single_output.isalpha():
   ⋮
  elif single_output.isdigit():
   ⋮
  elif single_output.isalnum():
   ⋮
  else:
   ⋮
