### $`\textcolor{GoldenRod}{\text{影像辨識並分類至不同資料夾}}`$

本 Python 應用程式，提供使用者，可將數以千計的影像檔，先行放置於特定資料夾當中，然後在執行期間，自動將那些影像檔，複製到不同分類的資料夾當中。

所以，本 Python 應用程式的不同分類，包括如下 4 種的影像檔內容：
```
1. 僅僅內含 英文字母 和 符號。
2. 僅僅內含 數字 和 小數點。
3. 混合了「數字、小數點」與「英文字母 和 符號」。
4. 無法正確辨識的內容。
```

本應用程式成功辨識之後，便會將已完成辨識的影像檔，複製到不同分類的資料夾裡面。

### $`\textcolor{GoldenRod}{\text{載入相關模組。}}`$
```
import re
from os import listdir
from shutil import copy
import pytesseract
```

### $`\textcolor{GoldenRod}{\text{開發者必須先在其個人電腦上，先行安裝好，用於影像辨識的 tesseract.exe}}`$
```
pytesseract.pytesseract.tesseract_cmd = r'C:/Users/Your_Name/AppData/Local/Programs
/Tesseract-OCR/tesseract.exe'
```

### $`\textcolor{GoldenRod}{\text{決定好等待被分類之數以千計的影像檔，所存放的路徑資料夾，以及分類資料夾。}}`$
```
filenames = listdir('images')
origin = 'images/'
destination = 'categorized_result/'
```

### $`\textcolor{GoldenRod}{\text{正式進行影像辨識，並根據其分類，複製該影像檔，至特定分類的資料夾。}}`$
```
# 此部分共有 13 列，在此僅提供一部分的程式碼。
for filename in filenames:
 ⋮
  single_output = re.sub(r'[\s\x21-\x2f\x3a-\x40\x5b-\x60\x7b-\x7e]', '', single_output)

  if single_output.isalpha():
   ⋮
  elif single_output.isdigit():
   ⋮
  elif single_output.isalnum():
   ⋮
  else:
   ⋮

```

### $`\textcolor{GoldenRod}{\text{該作品的參考網址}}`$

[影像辨識 並分類至 不同資料夾](https://gitlab.com/earth.taiwan.man/image-categorization/-/blob/main/demo-source-code.py)
